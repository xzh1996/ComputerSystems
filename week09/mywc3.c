#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int countChar(char input[]){
    int p_input = 0;
    int count = 0;
    int word = 0;
    char ch;

    while(p_input < strlen(input)){
        ch = input[p_input];
        if(ch==' '){
            if(word){ //读取到空字符，而之前是非空字符，则说明读完了一个单词
                count++;
                word = 0;
            }
        }else{ //读取到第一个非空字符，说明是单词的开始
            word = 1;
        }
        p_input++;
    }
    return count;
}

int main()
{
    char input[256];
    int p_input = 0;
    int count = 0;
    FILE *fp = fopen("text.txt","r");

    fgets(input,256,fp);
    fclose(fp);

    count = countChar(input);
    printf("Your words count:\n%d\n",count);

    return 0;
}
